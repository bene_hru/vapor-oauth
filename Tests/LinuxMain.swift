import XCTest

import Vapor_OAuthTests

var tests = [XCTestCaseEntry]()
tests += Vapor_OAuthTests.allTests()
XCTMain(tests)