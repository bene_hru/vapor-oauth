import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(Vapor_OAuthTests.allTests),
    ]
}
#endif