//
//  OAuthContent.swift
//  Async
//
//  Created by Benedikt Hruschka on 26.12.18.
//

import Foundation
import Vapor

struct OAuthContent {

    // MARK: Public properties
    let oauthConfiguration: OAuthConfiguration
    let parameters: [String: String]
    let method: HTTPMethod
    let url: URLRepresentable

    var headers = HTTPHeaders()

    // MARK: Private properties
    private let oauthNonce: String
    private let oauthTimestamp: Int

    init(oauthConfiguration: OAuthConfiguration, url: URLRepresentable, method: HTTPMethod) throws {
        self.oauthConfiguration = oauthConfiguration
        self.url = url
        self.parameters = url.queryParameters ?? [:]
        self.method = method
        
        oauthNonce = UUID().description.replacingOccurrences(of: "-", with: "").base64URLEscaped()
        oauthTimestamp = Int(Date().timeIntervalSince1970.rounded())
        guard let oauthSignature = generateSignature() else {
            throw Error.signature
        }

        var oauthParameter: [String] = []
        oauthParameter.append("oauth_consumer_key=\"" + oauthConfiguration.consumerKey + "\"")
        oauthParameter.append("oauth_nonce=\"" + oauthNonce + "\"")
        oauthParameter.append("oauth_signature=\"" + oauthSignature + "\"")
        oauthParameter.append("oauth_signature_method=\"" + oauthConfiguration.signatureMethod + "\"")
        oauthParameter.append("oauth_timestamp=\"\(oauthTimestamp)\"")
        oauthParameter.append("oauth_token=\"" + oauthConfiguration.accessToken + "\"")
        oauthParameter.append("oauth_version=\"\(oauthConfiguration.version)\"")

        let oauth = "OAuth \(oauthParameter.joined(separator: ", "))"

        headers = HTTPHeaders()
        headers.add(name: .authorization, value: oauth)
    }

    private func generateSignature() -> String? {
        var dataList: [String] = []

        dataList.append("oauth_consumer_key=\(oauthConfiguration.consumerKey)")
        dataList.append("oauth_nonce=\(oauthNonce)")
        dataList.append("oauth_signature_method=\(oauthConfiguration.signatureMethod)")
        dataList.append("oauth_timestamp=\(oauthTimestamp)")
        dataList.append("oauth_token=\(oauthConfiguration.accessToken)")
        dataList.append("oauth_version=\(oauthConfiguration.version)")

        parameters.forEach { parameter in
            guard let value = parameter.value.stringByAddingPercentEncodingForRFC3986() else { return }
            dataList.append("\(parameter.key)=\(value)")
        }

        var data = dataList.joined(separator: "&")

        guard let encodedData = data.stringByAddingPercentEncodingForRFC3986() else { return nil }
        guard let encodedUrl = url.urlStringWithoutQuery?.stringByAddingPercentEncodingForRFC3986() else { return nil }

        let httpMethod = method.string.uppercased()

        data = [httpMethod, encodedUrl, encodedData].joined(separator: "&")

        let signingKey = [oauthConfiguration.consumerSecretKey,
                          oauthConfiguration.accessTokenSecret].joined(separator: "&")

        let hmacResult = data.hmac(algorithm: .SHA1, key: signingKey)

        return hmacResult.stringByAddingPercentEncodingForRFC3986()
    }
}
