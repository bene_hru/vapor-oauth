//
//  URLRepresentable+Coding.swift
//  Vapor-OAuth
//
//  Created by Benedikt Hruschka on 26.12.18.
//

import Foundation
import Vapor

extension URLRepresentable {

    public func stringByAddingPercentEncodingForRFC3986() -> String? {
        let base: String?

        if let urlString = self as? String {
            base = urlString
        } else if let url = self as? URL {
            base = url.absoluteString
        } else {
            return nil
        }

        guard let baseString = base else { return nil }

        let unreserved = "-._~"
        var allowed = CharacterSet.alphanumerics
        allowed.insert(charactersIn: unreserved)

        return baseString.addingPercentEncoding(withAllowedCharacters: allowed)
    }

    var queryParameters: [String: String]? {
        let base: URL?

        if let urlString = self as? String {
            base = URL(string: urlString)
        } else if let url = self as? URL {
            base = url
        } else {
            return nil
        }

        guard let baseUrl = base,
              let components = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true),
              let queryItems = components.queryItems else {
              return nil
        }

        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }

        return parameters
    }

    var urlStringWithoutQuery: String? {
        guard let url = self.convertToURL() else { return nil }

        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.query = nil
        return components?.url?.absoluteString
    }

}
