//
//  Client+OAuth.swift
//  Async
//
//  Created by Benedikt Hruschka on 26.12.18.
//

import Vapor
import CommonCrypto

extension Client {

    /// Sends an HTTP `Request` to a server with an optional configuration closure that will run before sending.
    ///
    /// - parameters:
    ///     - url: Something `URLRepresentable` that will be converted to a `URL`.
    ///            This `URL` should contain a scheme, hostname, and port.
    ///     - headers: `HTTPHeaders` to add to the request. Empty by default.
    ///     - beforeSend: An optional closure that can mutate the `Request` before it is sent.
    /// - returns: A `Future` containing the requested `Response` or an `Error`.
    public func oauthRequest(method: HTTPMethod,
                             url: URLRepresentable,
                             oauthConfig: OAuthConfiguration,
                             headers: HTTPHeaders = [:],
                             beforeSend: (Request) throws -> () = { _ in }) throws -> Future<Response> {

        let oauthContent = try OAuthContent(oauthConfiguration: oauthConfig,
                                            url: url,
                                            method: method)

        // Build headers
        var oauthHeaders = HTTPHeaders()
        headers.forEach { (name, value) in
            oauthHeaders.add(name: name, value: value)
        }
        oauthContent.headers.forEach { (name, value) in
            oauthHeaders.add(name: name, value: value)
        }

        return send(method, headers: oauthHeaders, to: url, beforeSend: beforeSend)
    }

}
