//
//  OAuthConfiguration.swift
//  Async
//
//  Created by Benedikt Hruschka on 26.12.18.
//

import Foundation

public struct OAuthConfiguration {

    public let consumerKey: String
    public let consumerSecretKey: String

    public let signatureMethod: String

    public let accessToken: String
    public let accessTokenSecret: String

    public let version: String // = "1.0"

    public init(consumerKey: String, consumerSecretKey: String, accessToken: String, accessTokenSecret: String) {
        self.consumerKey = consumerKey
        self.consumerSecretKey = consumerSecretKey
        self.accessToken = accessToken
        self.accessTokenSecret = accessTokenSecret

        self.signatureMethod = "HMAC-SHA1"
        self.version = "1.0"
    }
}
