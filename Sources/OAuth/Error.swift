//
//  Error.swift
//  Vapor-OAuth
//
//  Created by Benedikt Hruschka on 26.12.18.
//

import Foundation

enum Error: Swift.Error {
    case signature
}
